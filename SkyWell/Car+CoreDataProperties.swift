//
//  Car+CoreDataProperties.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 16.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Foundation
import CoreData


extension Car {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Car> {
        return NSFetchRequest<Car>(entityName: "Car");
    }

    @NSManaged public var model: String?
    @NSManaged public var price: String?
    @NSManaged public var engine: String?
    @NSManaged public var transmission: String?
    @NSManaged public var condition: String?
    @NSManaged public var descript: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var images: NSData?

}
