//
//  imageAndButtonCollectionViewCell.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 18.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

protocol CollectionButtoProtocol:class{
    func collectionAction(forButton button:UIButton)
}


class ImageAndButtonCollectionViewCell: UICollectionViewCell {

    weak var delegate: CollectionButtoProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func buttonAction(_ sender: UIButton) {
    
        self.delegate?.collectionAction(forButton: sender)
    }
    
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!
}
