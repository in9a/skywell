//
//  WeatherData+CoreDataClass.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 17.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Foundation
import CoreData


public class WeatherData: NSManagedObject {
    convenience init(){
    
        let entity = NSEntityDescription.entity(forEntityName: CoreDataEntity.weather, in: CoreDataManager.sharedInstance.managedObjectContext)
        
        self.init(entity: entity!, insertInto: CoreDataManager.sharedInstance.managedObjectContext)
        
    }
    
}


