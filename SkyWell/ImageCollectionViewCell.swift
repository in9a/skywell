//
//  ImageCollectionViewCell.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 15.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!{
        didSet{
            imageView.contentMode = .scaleAspectFill
        }
    }
    @IBOutlet var widthConstraint: NSLayoutConstraint!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    
    @IBOutlet var constWidthtConstraint: NSLayoutConstraint!
    @IBOutlet var constHeightConstraint: NSLayoutConstraint!
    
    var isLast:Bool = true
        {
        didSet{
            heightConstraint.isActive = !isLast
            widthConstraint.isActive = !isLast
            constWidthtConstraint.isActive = isLast
            constHeightConstraint.isActive = isLast
            
            if isLast == true {
                print("newValue")
                imageView.image = #imageLiteral(resourceName: "plus")
                layer.borderWidth = 0.5
                layer.borderColor = UIColor.white.cgColor
            } else {
                print("isLastNewValue NO")
                imageView.image = nil
                layer.borderWidth = 1
                layer.borderColor = UIColor.white.cgColor
            }
            
            self.setNeedsLayout()
            self.layoutIfNeeded()
            self.layoutSubviews()
        }
    }
    
 }
