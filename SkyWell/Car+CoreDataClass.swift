//
//  Car+CoreDataClass.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 16.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit
import CoreData


public class Car: NSManagedObject {

    convenience init(){
        
        let entity = NSEntityDescription.entity(forEntityName: CoreDataEntity.car, in: CoreDataManager.sharedInstance.managedObjectContext)
        
        self.init(entity: entity!, insertInto: CoreDataManager.sharedInstance.managedObjectContext)
        
    }
    
    func arrayOfImagesFor() -> [UIImage]? {
        
        if let data = self.images as? Data {
            
            if let mySavedData = NSKeyedUnarchiver.unarchiveObject(with: data) as? [NSData]{
                
                var result = [UIImage]()
                
                for items in mySavedData{
                    if let image = UIImage(data: items as Data){
                        result.append(image)
                    }
                }
                return result
            }
            
        }
        return nil
    }
    
    func saveAllImages(fromArray selectedImageArray:[UIImage]) -> Data{
        
        var result = [NSData]();
        
        for img in selectedImageArray {
            let data : NSData = NSData(data: UIImagePNGRepresentation(img)!)
            result.append(data)
        }
        
        //convert the Array to NSData
        return NSKeyedArchiver.archivedData(withRootObject: result)
    }
}
