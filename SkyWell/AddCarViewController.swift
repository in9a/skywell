//
//  AddCarViewController.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 14.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit
import CoreData


enum CellTypes: Int {
    case collectionCell, carModel, carPrice, engineCell, transmissionCell, conditionCell, descriptionCell
}

class AddCarViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.backgroundColor = UIColor.greenMainColor()
        }
    }
    
    let rowHeight:CGFloat = 50
    var selectedTextField:UITextField?
    var selectedTextView:UITextView?
    var selectedIndex: Int?
    
    lazy var toolBar :UIToolbar = {
        let toolBar = CustomToolBar(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 44) , customDelegate: self)
        
        return toolBar
    }()
    
    var collecionHeight:CGFloat {
        return self.tableView.frame.size.height * 0.25
    }
    
    var selectedImageArray = [UIImage](){
        didSet{
            tableView.beginUpdates()
            
            let indexPath = IndexPath(row: CellTypes.collectionCell.hashValue, section: 0)
            tableView.reloadRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
    
    var tableViewDataSource:[CellTypes] = [.collectionCell, .carModel, .carPrice, .engineCell, .transmissionCell, .conditionCell, .descriptionCell] {
        didSet{
            tableView.reloadData()
        }
    }
    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationTitle()
        
        allowNotification()
    }
    
    internal func setupNavigationTitle(){
        navigationController?.navigationBar.isTranslucent = false
        
        let titleLabel = UILabel(frame: CGRect.init(x: 0, y: 0, width: 100, height:30))
        titleLabel.textAlignment = .right
        titleLabel.isUserInteractionEnabled = true
        
        
        titleLabel.text = NSLocalizedString("AddCar", comment: "")
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont.boldSystemFont(ofSize: 22)
        titleLabel.sizeToFit()
        titleLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addCarAction)))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: titleLabel)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    lazy var carToSave:CarModel  = {
        
        return CarModel.emptyCar()
        
    }()
    
    //MARK: Action
    func addCarAction(){
        
        if  carToSave.model.isEmpty {
            let alert = UIAlertController(title: NSLocalizedString("enterModel", comment: ""), message: "", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(alertAction)
            
            self.present(alert, animated: true, completion: nil)
        }
        else {
            saveToCoreData()
        }
    }
    
    func saveToCoreData(){
        let carData = Car()
        
        carData.model = carToSave.model
        carData.price = carToSave.price
        carData.engine = carToSave.engine
        carData.transmission = carToSave.transmission
        carData.condition = carToSave.condition
        carData.descript = carToSave.descript
        carData.date = NSDate()
        
        //Save images
        if let data = carData.saveAllImages(fromArray: selectedImageArray) as NSData?{
            print("saved images")
            carData.images = data
        }
        
        CoreDataManager.sharedInstance.saveContext()
        
        if let nvc = self.navigationController{
            nvc.popViewController(animated: true)
        }
        
        //        let array = CarModel.fetchAllCars()
        //        print(array)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.pickerSegue, let index = sender as? IndexPath {
            
            let vc = segue.destination as! PickerViewController
            vc.delegate = self
            
            switch index.row {
            case CellTypes.engineCell.hashValue:
                vc.typeOfPicker = .engineCell
            case CellTypes.transmissionCell.hashValue:
                vc.typeOfPicker = .transmissionCell
            case CellTypes.conditionCell.hashValue:
                vc.typeOfPicker = .conditionCell
            default:break
            }
        }
    }
}

//MARK: TableView
extension AddCarViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewDataSource.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let tableViewCell = cell as? CollectionTableViewCell else { return }
        
        tableViewCell.setCollectionViewDataSourceDelegate(dataSourceDelegate: self, delegate: self, forRow: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        let description = self.tableView.frame.size.height - collecionHeight - (5 * rowHeight)
        
        switch indexPath.row {
        case CellTypes.collectionCell.hashValue:
            return collecionHeight
            
        case CellTypes.descriptionCell.hashValue:
            return  description
            
            
        default: return rowHeight
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        switch indexPath.row {
        case CellTypes.engineCell.hashValue, CellTypes.transmissionCell.hashValue, CellTypes.conditionCell.hashValue:
            
            self.performSegue(withIdentifier: SegueIdentifier.pickerSegue, sender: indexPath)
            
            break
            
        default:  break
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case CellTypes.collectionCell.hashValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableCellCollectionId) as! CollectionTableViewCell
            return cell
            
            
        case CellTypes.carModel.hashValue, CellTypes.carPrice.hashValue:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableDefaultCellID) as! SubtitleTableViewCell
            cell.setUpCell(indexPath.row)
            cell.textField?.delegate = self
            cell.textField?.inputAccessoryView = toolBar
            return cell
            
            
        case CellTypes.descriptionCell.hashValue :
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableTextViewCellId) as! TextViewTableViewCell
            cell.textView.delegate = self
            cell.textView.inputAccessoryView = toolBar
            cell.textView.tag = CellTypes.descriptionCell.hashValue
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableActionCellId) as! ActionTableViewCell
            cell.setUpCell(indexPath.row)
            
            
            return cell
        }
    }
}

//MARK: Text and keyboard handling
extension AddCarViewController:ToolBarProtocol{
    
    func donePressed(){
        if let textField = selectedTextField{
            textField.resignFirstResponder()
        }
        else if let textView = selectedTextView{
            textView.resignFirstResponder()
        }
        
        tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
    }
    
    func cancelPressed(){
        if let textField = selectedTextField{
            textField.text = ""
            textField.resignFirstResponder()
        }
        
        if let textView = selectedTextView{
            textView.text = ""
            textView.resignFirstResponder()
        }
    }
    
    func allowNotification(){
        let appear = Notification.Name("UIKeyboardWillShowNotification")
        let dismiss = Notification.Name("UIKeyboardWillHideNotification")
        
        // Register to receive notification
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: appear, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: dismiss, object: nil)
    }
    
    func keyboardWillShow(_ notif :Notification){
        
        if let userInfo = notif.userInfo {
            if let keyboardSize = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                let contentInsets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
                tableView.contentInset = contentInsets
                tableView.scrollIndicatorInsets = contentInsets
                
                if let index = selectedIndex{
                    let indexPath = IndexPath(row: index, section: 0)
                    tableView.scrollToRow(at: indexPath, at: .middle, animated: true)
                }
            }
        }
    }
    
    func keyboardWillHide(_ notif :Notification){
        tableView.contentInset = UIEdgeInsets.zero
        tableView.scrollIndicatorInsets = UIEdgeInsets.zero
        tableView.scrollsToTop = true
    }
}

extension AddCarViewController: UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        selectedTextView = textView
        selectedIndex = textView.tag
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        selectedTextView = nil
        selectedIndex = nil
        
        carToSave.descript = textView.text
    }
}

extension AddCarViewController: UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTextField = textField
        selectedIndex = textField.tag
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.tag == CellTypes.carModel.hashValue{
            let index = IndexPath(row: CellTypes.carPrice.hashValue, section: 0)
            let cell = tableView.cellForRow(at: index) as! SubtitleTableViewCell
            cell.textField?.delegate = self
            cell.textField?.becomeFirstResponder()
            return true
        } else {
            textField.resignFirstResponder()
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        selectedTextField = nil
        selectedIndex = nil
        
        
        switch textField.tag {
        case CellTypes.carModel.hashValue:
            carToSave.model = textField.text ?? ""
        case CellTypes.carPrice.hashValue:
            carToSave.price = textField.text ?? "0"
        default:break
        }
    }
}

extension AddCarViewController: PickerDelegate{
    func doneWith(string:String, andType:Int){
        
        let index = IndexPath(row: andType, section: 0)
        
        if let cell = tableView.cellForRow(at: index) as? ActionTableViewCell{
            cell.valueLabel.text = string

            switch andType {
            case CellTypes.engineCell.hashValue:
                carToSave.engine = string
            case CellTypes.transmissionCell.hashValue:
                carToSave.transmission = string
            case CellTypes.conditionCell.hashValue:
                carToSave.condition = string
            default:
                break
            }
            
        }
        
    }
}
