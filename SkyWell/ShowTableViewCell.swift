//
//  ShowTableViewCell.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 16.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

class ShowTableViewCell: BaseCell {

    @IBOutlet var priceValueLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var modelNameLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    
    override func updateCell(withCar model:Car){

        priceLabel.text = NSLocalizedString("Price", comment: "")
        titleLabel.text = NSLocalizedString("Car", comment: "")
        modelNameLabel.text = model.model
        priceValueLabel.text = model.price
        
        if (model.price?.isEmpty)! {
            priceValueLabel.text = NSLocalizedString("noPrice", comment: "")
        }
    }
    
}
