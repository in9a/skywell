//
//  CarTableViewCell.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 14.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

class CarTableViewCell: BaseCell {

    let carImageView: UIImageView = {
        let imageView = UIImageView()
    
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.layer.cornerRadius = 5
        imageView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.image = #imageLiteral(resourceName: "car")
        return imageView
    }()
    
    let priceLabel: UILabel = {
        let title = UILabel()
        title.font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightMedium)
        title.text = "20 000"
        title.textColor = UIColor.white
        return title
    }()
    
    let nameLabel: UILabel = {
        let title = UILabel()
        title.font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightMedium)
        title.text = "AUDI ASGAGH ASLHGA agalkhgahgag'ka"
        title.textColor = UIColor.white
        return title
    }()
     
    let stack:UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.backgroundColor = UIColor.red
        stackView.spacing = 2
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        return stackView
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        
        setupViews()
    }
    
    private func setupViews() {
        self.backgroundColor = UIColor.greenMainColor()
        tintColor = UIColor.white
        
        self.accessoryType = .disclosureIndicator
        
        
        
        addSubview(carImageView)
        addSubview(stack)
        
        carImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        carImageView.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        carImageView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        self.addConstraint(NSLayoutConstraint(
            item: carImageView,
            attribute: .width,
            relatedBy: .equal,
            toItem: self.contentView,
            attribute: .height,
            multiplier: 1.3, constant: 0))
        
    //StackView
        
        let price = UILabel()
        price.adjustsFontSizeToFitWidth = true
        price.font = UIFont.systemFont(ofSize: 10, weight: UIFontWeightThin)
        price.text = NSLocalizedString("Price", comment: "")
        price.textColor = UIColor.white
        
        let car = UILabel()
        car.adjustsFontSizeToFitWidth = true
        car.font = UIFont.systemFont(ofSize: 10, weight: UIFontWeightThin)
        car.text = NSLocalizedString("Car", comment: "")
        car.textColor = UIColor.white
        
        stack.addArrangedSubview(price)
        stack.addArrangedSubview(priceLabel)
        stack.addArrangedSubview(car)
        stack.addArrangedSubview(nameLabel)
    
        
        addConstraintsWithFormat("H:|[v0]-[v1]-30-|", views:carImageView, stack)
        addConstraintsWithFormat("V:|-2-[v0]-2-|", views: stack)
         
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateCell(withCar model:Car){
        
        if let images = model.arrayOfImagesFor(), images.count > 0{
            self.carImageView.image = images.first
        }
        
        priceLabel.text = model.price
        
        if (model.price?.isEmpty)! {
            priceLabel.text = NSLocalizedString("noPrice", comment: "")
        }
        
        nameLabel.text = model.model
        
    }
}

class BaseCell: UITableViewCell {
    func updateCell(withCar model:Car){
        
    }
   
    override var isSelected: Bool{
        didSet{
            backgroundColor = UIColor.greenMainColor()
        }
    }
}


