//
//  WeatherData+CoreDataProperties.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 17.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import Foundation
import CoreData


extension WeatherData {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeatherData> {
        return NSFetchRequest<WeatherData>(entityName: "WeatherData");
    }

    @NSManaged public var temperature: String?
    @NSManaged public var descript: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var icon: NSData?

}
