//
//  LocationManager.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 14.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit
import CoreLocation

typealias SuccessCallback = (CLLocation) -> ()
typealias FailureCallback = (LocationError) -> ()

enum LocationError {
    case statusDenied, failedToRecive
}

protocol LocationManagerProtocol {
    func getCurrentLocation(success: @escaping SuccessCallback, failure: @escaping FailureCallback)
}

class LocationManager: NSObject, CLLocationManagerDelegate, LocationManagerProtocol {
    let locationManager: CLLocationManager
    var success: SuccessCallback?
    var failure: FailureCallback?
    
    init(locationManager: CLLocationManager) {
        self.locationManager = locationManager
        super.init()
        locationManager.delegate = self
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let successClosure = success, let location = locations.first {
            successClosure(location)
        }
    
        success = nil
        failure = nil
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let failureClosure = failure {
            failureClosure(LocationError.failedToRecive)
        }
        success = nil
        failure = nil
    }
    
    func getCurrentLocation(success: @escaping SuccessCallback, failure: @escaping FailureCallback) {
        self.success = success
        self.failure = failure
        
        switch CLLocationManager.authorizationStatus() {
      
        case .denied: failure(LocationError.statusDenied)
       
        default:
            locationManager.requestWhenInUseAuthorization()
            //locationManager.startUpdatingLocation()
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse || status == .authorizedAlways {
            locationManager.requestLocation()
        }
    }
}

class LocationManagerFactory {
        static func defaultLocationManager() -> LocationManager {
            return LocationManager(locationManager: CLLocationManager())
        }
}
