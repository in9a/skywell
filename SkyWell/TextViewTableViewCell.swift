//
//  TextViewTableViewCell.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 15.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

class TextViewTableViewCell: BaseCell {

    
    var selectedTextView: UITextView?
    
    @IBOutlet var descriptionLabel: UILabel! {
        didSet{
            descriptionLabel.text = NSLocalizedString("descript", comment: "")
        }
    }
    @IBOutlet var textView: UITextView!{
        didSet{
            textView.backgroundColor = UIColor.greenMainColor()
            textView.textColor = UIColor.white
            textView.scrollsToTop = true
            textView.text = ""
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = UIColor.greenMainColor()
    }
    
    override func updateCell(withCar model:Car){
        if  model.description.isEmpty{
            self.selectedTextView?.text = NSLocalizedString("No description entered", comment: "")
        }else {
            self.selectedTextView?.text = model.description
        } 
    }

}

