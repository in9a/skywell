//
//  ActionTableViewCell.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 15.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

class ActionTableViewCell: SubtitleTableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var currentType:Int?
    
    
    override func  updateCell(withCar model: Car){
        
        if let type = currentType{
            switch type {
            case CellTypes.engineCell.hashValue:
                valueLabel.text = model.engine
            case CellTypes.transmissionCell.hashValue:
                valueLabel.text = model.transmission
            case CellTypes.conditionCell.hashValue:
                valueLabel.text = model.condition
            default:
                break
            }
            
        }
    }
}
