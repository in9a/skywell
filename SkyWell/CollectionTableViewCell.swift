//
//  CollectionTableViewCell.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 15.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

class CollectionTableViewCell: BaseCell {

    @IBOutlet weak var collectionView: UICollectionView!
    
    func setCollectionViewDataSourceDelegate (dataSourceDelegate: UICollectionViewDataSource?, delegate:UICollectionViewDelegate?, forRow row: Int) {
        backgroundColor = UIColor.greenMainColor()
        
        collectionView.delegate = delegate
        collectionView.dataSource = dataSourceDelegate
        collectionView.tag = row
        collectionView.backgroundColor = UIColor.greenMainColor()
        
        
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0, y: collectionView.frame.size.height - 1, width: collectionView.frame.size.width, height: 1)
        bottomBorder.backgroundColor = UIColor.white.cgColor
        collectionView.layer.addSublayer(bottomBorder)
        
        if let fl = collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            fl.scrollDirection = .horizontal
        }  
        
        collectionView.reloadData()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
