//
//  WeatherDataModel.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 14.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit
import CoreLocation

protocol WeatherDataModelDelegate: class {
    func update(withWeather weather:Weather, andDataModel:WeatherDataModel)
    func showAlertForDeniedStatus()
    func showAlert(forNetworkError message:String)
    func update(withCity city:String)
}

class WeatherDataModel: NSObject {

    weak var delegate: WeatherDataModelDelegate?
    private let locationManager: LocationManager
    private let networkManager: NetworkManager
    private let coreDataManager = CoreDataManager.sharedInstance
    private var weather: Weather?
    
    private var location:CLLocation?
    
    init(locationManager:LocationManager, networkManager:NetworkManager, delegate: WeatherDataModelDelegate?) {
        self.locationManager = locationManager
        self.networkManager = networkManager
        self.delegate = delegate
    }
    
    func updateData(){
        
        
        locationManager.getCurrentLocation(
            success: {[weak self] (location) in
                
                guard let strongSelf = self else {return}
                
                strongSelf.location = location
                strongSelf.updateCity()
                
                strongSelf.networkManager.getWeatherForCoordinate( location.coordinate,
                    success: {[weak self]  (weather) in
                        guard let strongSelf = self else {return}
                        
                        strongSelf.weather = weather
                        
                        strongSelf.weatherDidUpdate()
                    },
                    failure: { (networkError) in
                        strongSelf.showAlertFornetworkError()
                        
                        strongSelf.weather = Weather.lastSavedWeather()
                        
                        strongSelf.weatherDidUpdate()
                })
            })
        { [weak self] (locationError) in
            guard let strongSelf = self else {return}
            
            switch locationError{
                case .statusDenied:
                    strongSelf.showAlert()
                   
                    strongSelf.weather = Weather.lastSavedWeather()
                    
                    strongSelf.delegate?.update(withWeather: strongSelf.weather!, andDataModel: strongSelf)
                
                case .failedToRecive:
                    strongSelf.weather = Weather.lastSavedWeather()
                    
                    strongSelf.weatherDidUpdate()
                break
            }
        }
    }
 
    
    private func weatherDidUpdate() {
        weather!.saveLast()
        
        DispatchQueue.main.async {
            self.delegate?.update(withWeather: self.weather!, andDataModel: self)
        }
    }
    
    private func showAlert(){
        DispatchQueue.main.async {
            self.delegate?.showAlertForDeniedStatus()
        }
    }
    
    private func showAlertFornetworkError(){
        
        DispatchQueue.main.async {
            self.delegate?.showAlert(forNetworkError: NSLocalizedString("network", comment: ""))
        }
    }
    
    private func updateCity(){
        
        if let loc = self.location {
            CLGeocoder().reverseGeocodeLocation(loc, completionHandler: { [weak self] (places, error) in
                
                guard let strongSelf = self else {return}
                
                if let currentPlace = places?.first, let place =  currentPlace.addressDictionary?["City"] as? String {
                    
                    print(place)
                    
                    DispatchQueue.main.async {
                        strongSelf.delegate?.update(withCity: place)
                    }
                }
            })
        }
        
    }
}


class WeatherDataModelFactory{
    
    static func defaultWeatherDataWithDeleagte(_ delegate :WeatherDataModelDelegate?) -> WeatherDataModel{
        
        let data = WeatherDataModel(
            locationManager: LocationManagerFactory.defaultLocationManager(),
            networkManager: NetworkManagerImpl(),
            delegate: delegate)
        
        return data
    }
    
}
