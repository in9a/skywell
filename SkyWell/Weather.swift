//
//  WeatherData.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 14.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit
import CoreData

struct Weather {
    
    var temperature: String
    var icon: UIImage
    var description: String
//    var unitSystem: MesureSystem
    
    static func decode(withJSON json:[String: AnyObject], forMesureSystem units:MesureSystem) -> Weather {
        
        guard
            let temperature = json["main"]!["temp"] as? Double,
            let weath = json["weather"] as? [[String:AnyObject]],
            let icon = weath.first?["icon"] as? String,
            let descr = weath.first?["description"] as? String else
        {
            return emptyWeather()
        }
        
        let image = UIImage(named: icon) ?? #imageLiteral(resourceName: "none")
        
        var symbol = "F"
        if units == .metric {
            symbol = "C"
        }
        
        let temp = "\(Int(temperature)) \(symbol)"
        
        return Weather(temperature: temp, icon: image, description: descr)
    }
    
    static func emptyWeather() -> Weather {
        return Weather(temperature: "", icon: #imageLiteral(resourceName: "none"), description: NSLocalizedString("Weather", comment: ""))
    }
    
    
    func saveLast(){
        let fetchRequest: NSFetchRequest<WeatherData> = WeatherData.fetchRequest()
        
        do {
            let fetchedData = try CoreDataManager.sharedInstance.managedObjectContext.fetch(fetchRequest)
            
            for data in fetchedData{
                CoreDataManager.sharedInstance.managedObjectContext.delete(data)
            }
            
        } catch {
            
        }
        let weatherCD = WeatherData()
        
        weatherCD.date = NSDate()
        weatherCD.temperature = self.temperature
        weatherCD.descript = self.description
        weatherCD.icon = UIImagePNGRepresentation(self.icon) as NSData?
        weatherCD.date = NSDate()
        
        CoreDataManager.sharedInstance.saveContext()
 
    }
    
    static func lastSavedWeather() -> Weather{
        
        let fetchRequest: NSFetchRequest<WeatherData> = WeatherData.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            let fetchedData = try CoreDataManager.sharedInstance.managedObjectContext.fetch(fetchRequest)
            
            if (fetchedData.isEmpty) {
                return emptyWeather()
            }
            else {
                print(fetchedData)
                if let weaterSaved = fetchedData.first {
                    
                    guard
                        let im = UIImage(data: weaterSaved.icon as! Data),
                        let temp = weaterSaved.temperature,
                        let des = weaterSaved.descript
                        else {
                            return emptyWeather()
                    }
                    return Weather(temperature: temp, icon: im, description: des)
                }
            }
            
        } catch {
            return emptyWeather()
        }
    
        return emptyWeather()
    }
}

public enum MesureSystem {
    case metric
    case imperial 
    
    func stringValue()->String{
        switch self {
        case .metric:
            return "metric"
        case .imperial:
            return "imperial"
        }
    }
}
 
