//
//  CarListHeaderView.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 14.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

class CarListHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var backgroundImage: UIImageView!{
        didSet{
            backgroundImage.image = #imageLiteral(resourceName: "weatherIcon")
        }
    }
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var cityLabel: UILabel!{
        didSet{
            cityLabel.text = ""
        }
    }
    @IBOutlet weak var descriptionLabel: UILabel!{
        didSet{
            descriptionLabel.text = ""
        }
    }
    @IBOutlet weak var temperatureLabel: UILabel!{
        didSet{
            temperatureLabel.text = ""
        }
    }
    @IBOutlet weak var tempImageView: UIImageView!
    
    func update(forWeather currentWeater:Weather){
        
        if activityIndicator.isAnimating{
            activityIndicator.stopAnimating()
        }
        
        descriptionLabel.text = currentWeater.description.capitalizingFirstLetter()
        temperatureLabel.text = currentWeater.temperature
        tempImageView.image = currentWeater.icon
        
        setNeedsDisplay()
        setNeedsLayout()
    }
    
}
