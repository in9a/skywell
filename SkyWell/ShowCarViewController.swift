//
//  ShowCarViewController.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 15.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

class ShowCarViewController: AddCarViewController, PageProtocol {
    
    var selectedCar: Car?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.isEditing = false
        tableView.allowsSelection = false
        tableView.register(UINib(nibName: Constants.tablePageCellID, bundle: nil), forCellReuseIdentifier: Constants.tablePageCellID)
        tableView.register(UINib(nibName: Constants.tableShowCellID, bundle: nil), forCellReuseIdentifier: Constants.tableShowCellID)
        
        
        if let car = selectedCar {
            if let array =  car.arrayOfImagesFor(), array.count > 0 {
                selectedImageArray = array
            } else {
                selectedImageArray = [#imageLiteral(resourceName: "defaultImage2")]
            }
        }
    }
    
    override func setupNavigationTitle() {
        super.setupNavigationTitle()
        
        navigationItem.rightBarButtonItem = nil
        
        navigationItem.title = NSLocalizedString("Car", comment: "")
    }
    
    func pageControlActiom(sender:UIPageControl!){
        
    }
    
    //MARK: - TableView
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let car = selectedCar{
            var cell = BaseCell()
            
            switch indexPath.row {
                
            case CellTypes.collectionCell.hashValue:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableCellCollectionId) as! CollectionTableViewCell
                if let collCell = cell as? CollectionTableViewCell{
                    collCell.collectionView.isPagingEnabled = true
                    collCell.collectionView.showsHorizontalScrollIndicator = false
                    
                    
                    let nib = UINib(nibName: Constants.collectionButtonCellId, bundle: nil)
                    collCell.collectionView.register(nib, forCellWithReuseIdentifier: Constants.collectionButtonCellId)
                }
                
            case CellTypes.carModel.hashValue:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.tablePageCellID) as! PagingViewTableViewCell
                if let c = cell as? PagingViewTableViewCell {
                    c.delegate = self
                }
                
            case  2:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableShowCellID) as! ShowTableViewCell
                
                
            case CellTypes.descriptionCell.hashValue :
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableTextViewCellId) as! TextViewTableViewCell
                cell.isUserInteractionEnabled = false
                
                
            default:
                cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableActionCellId) as! ActionTableViewCell
                if let c = cell as? ActionTableViewCell{
                    c.setUpCell(indexPath.row)
                    c.currentType = indexPath.row
                }
            }
            
            cell.updateCell(withCar: car)
            return cell
        }
        return UITableViewCell()
        
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let height = self.tableView.frame.size.height
        let collection = (self.tableView.frame.size.width * 0.75) + 10
        let page:CGFloat = 30
        let main:CGFloat = 50
        let other:CGFloat = 40
        let descr = height - collection - page - main - other*3
        
        switch indexPath.row {
            
        case CellTypes.collectionCell.hashValue:
            return collection
        case 1:
            return page
            
        case  2:
            return main
            
        case CellTypes.descriptionCell.hashValue :
            return descr
            
            
        default:
            return other
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: - Collection
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.collectionButtonCellId, for: indexPath) as! ImageAndButtonCollectionViewCell
        
        cell.delegate = self
        
        cell.backgroundImage.image = selectedImageArray[indexPath.row]
        cell.backgroundImage.contentMode = .scaleToFill
        
        cell.rightButton.tag = indexPath.row + 1
        cell.leftButton.tag = indexPath.row - 1
        
        if indexPath.row == 0 {
            cell.leftButton.isHidden = true
        }
        if indexPath.row == selectedImageArray.count - 1{
            cell.rightButton.isHidden = true
        }
        
        
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        cell.updateConstraints()
        cell.updateConstraintsIfNeeded()
        cell.layoutSubviews()
        
        
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedImageArray.count
    }
    
    //MARK: - Layout
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
                 return CGSize.init(width: collectionView.frame.size.width - 10, height: collectionView.frame.size.height - 10)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if let collection = scrollView as? UICollectionView{
           
            let index = IndexPath(row: 1, section: 0)
            if let cell = tableView.cellForRow(at: index) as? PagingViewTableViewCell{
                cell.pageControl.currentPage = Int(collection.contentOffset.x) / Int(collection.frame.size.width)
            }
        }
    }
    
    //MARK - Actions
    func pageControlAction(sender:UIPageControl!){
        if let cell = tableView.cellForRow(at: IndexPath(row: CellTypes.collectionCell.hashValue, section: 0)) as? CollectionTableViewCell{
            
            cell.collectionView.scrollToItem(
                at: IndexPath(row: sender.currentPage, section: 0),
                at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        }
    }
    
}

extension ShowCarViewController: CollectionButtoProtocol{

    func collectionAction(forButton button:UIButton){
        if let cell = tableView.cellForRow(at: IndexPath(row: CellTypes.collectionCell.hashValue, section: 0)) as? CollectionTableViewCell{
            
            cell.collectionView.scrollToItem(
                at: IndexPath(row: button.tag, section: 0),
                at: UICollectionViewScrollPosition.centeredHorizontally, animated: true)
        }
    }
    
}
