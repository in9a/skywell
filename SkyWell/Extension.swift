//
//  Extension.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 14.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

extension UIView {
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}

extension UIColor {
    static func rgb(red: CGFloat, green: CGFloat, blue: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
    }
    
    static func greenMainColor() -> UIColor{
        return UIColor.rgb(red: 180, green: 210, blue: 80)
    }
}

extension UIImage {
    
    static func imageWithColor(color: UIColor) -> UIImage {
        let pixelScale = UIScreen.main.scale
        let pixelSize = 1 / pixelScale
        let fillSize = CGSize(width: pixelSize, height: pixelSize)
        let fillRect = CGRect(origin: CGPoint.zero, size: fillSize)
        UIGraphicsBeginImageContextWithOptions(fillRect.size, false, pixelScale)
        let graphicsContext = UIGraphicsGetCurrentContext()
        graphicsContext!.setFillColor(color.cgColor)
        graphicsContext!.fill(fillRect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}


extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}


//Used to create toolbar
protocol ToolBarProtocol: class {
    func donePressed()
    func cancelPressed()
}

class CustomToolBar: UIToolbar {
    
    weak var customDelegate:ToolBarProtocol!
    
    init(frame: CGRect, customDelegate:ToolBarProtocol) {
        super.init(frame: frame)
        barStyle = UIBarStyle.default
        isTranslucent = true
        tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        self.customDelegate = customDelegate
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: .done, target: self, action: #selector(self.done))
        let cancelButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .plain, target: self, action: #selector(self.cancel))
        cancelButton.isEnabled = true
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        setItems([cancelButton, spaceButton, doneButton], animated: false)
        isUserInteractionEnabled = true
        sizeToFit()
 
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func done(){
        customDelegate!.donePressed()
    }
    
    func cancel() {
        customDelegate!.cancelPressed()
    }
}


