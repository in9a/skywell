//
//  PickerViewController.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 15.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

protocol PickerDelegate: class {
    func doneWith(string:String, andType:Int)
}

class PickerViewController: UIViewController {
//    
//    enum PickerType: Int {
//        case engineType = 3
//        case transmissionType = 4
//        case conditionType = 5
//    }
    
    var typeOfPicker:CellTypes = CellTypes.engineCell {
        didSet{
            
        }
    }
    
    var pickerDataSource = [String]()
    
    weak var delegate: PickerDelegate?
    
    @IBOutlet weak var toolBar: UIToolbar!
    
    @IBOutlet var pickerView: UIPickerView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch typeOfPicker {
        case .engineCell:
            
            var engine = 1.0
            for _ in 1...10 {
                engine += 0.2
                pickerDataSource.append(String(engine))
            }
        case .transmissionCell:
            pickerDataSource = [NSLocalizedString("good", comment: ""), NSLocalizedString("used", comment: ""), NSLocalizedString("new", comment: "")]
        case .conditionCell:
            pickerDataSource = [NSLocalizedString("auto", comment: ""), NSLocalizedString("manual", comment: ""), NSLocalizedString("robot", comment: "")]
        default: break
        }
    }
 
    
    @IBAction func cancelPressed(_ sender: AnyObject){
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func donePressed(_ sender: AnyObject) {
        donePressed()
    }
    
    func donePressed(){
        
        if let del = self.delegate{
            let choosenString = self.pickerDataSource[self.pickerView.selectedRow(inComponent: 0)]
            
            
            switch self.typeOfPicker {
                case .engineCell: del.doneWith(string: choosenString, andType: CellTypes.engineCell.hashValue)
                case .transmissionCell: del.doneWith(string: choosenString, andType: CellTypes.transmissionCell.hashValue)
                case .conditionCell: del.doneWith(string: choosenString, andType: CellTypes.conditionCell.hashValue)
            default: break
            }
        }
        
        dismiss(animated: true) { }
    }
}

extension PickerViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerDataSource[row]
    }
}
