//
//  ViewController.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 14.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit
import CoreData

struct Constants {
    //table cells
    static let headerIdentifier = "CarListHeaderView"
    static let tableCellCarId = "CarTableViewCell"
    static let tableCellCollectionId = "CollectionTableViewCell"
    static let tableDefaultCellID = "SubtitleTableViewCell"
    static let tableActionCellId = "ActionTableViewCell"
    static let tableTextViewCellId = "TextViewTableViewCell"
    static let tablePageCellID = "PagingViewTableViewCell"
    static let tableShowCellID = "ShowTableViewCell"
    
    //collection cells
    static let collectionImageIdentifier = "ImageCollectionViewCell"
    static let collectionButtonCellId = "ImageAndButtonCollectionViewCell"
    
    //car list vc
    static let cellMainHeigt: CGFloat = 60
    static let headerMainHeight: CGFloat = 200
    
}

struct SegueIdentifier {
    static let addSegue = "showAdd"
    static let detilSegue = "showDetail"
    static let pickerSegue = "showPicker"
    static let showSegue = "showCar"
}

class CarListViewController: UIViewController {
    
    
    var fetchedResultsController:NSFetchedResultsController<Car> = {
        let fetchRequest: NSFetchRequest<Car> = NSFetchRequest(entityName: CoreDataEntity.car)
        
        let sortDescriptor = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let fetchedResultsController = NSFetchedResultsController(
            fetchRequest: fetchRequest,
            managedObjectContext: CoreDataManager.sharedInstance.managedObjectContext,
            sectionNameKeyPath: nil,
            cacheName: nil)
        
        return fetchedResultsController
    }()
    
    
    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.backgroundColor = UIColor.greenMainColor()
            
            tableView.delegate = self
            tableView.dataSource = self
            tableView.separatorColor = UIColor.white
            
            self.automaticallyAdjustsScrollViewInsets = false
            
            let headerNib = UINib(nibName: Constants.headerIdentifier, bundle: nil)
            tableView.register(headerNib, forHeaderFooterViewReuseIdentifier: Constants.headerIdentifier)
            
            // let cellNib = UINib(nibName: Constants.cellIdentifier, bundle: nil)
            //tableView.register(cellNib, forCellReuseIdentifier: Constants.cellIdentifier)
            tableView.register(CarTableViewCell.self, forCellReuseIdentifier: Constants.tableCellCarId)
        }
    }
    
    lazy var weatherDataModel: WeatherDataModel = {
        return WeatherDataModelFactory.defaultWeatherDataWithDeleagte(self)
    }()
    
    // MARK: Life
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchedResultsController.delegate = self
        
        setupNavigation()
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print(error)
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        weatherDataModel.updateData()
    }
    
    // MARK: Action
    func addCarAction() {
        self.performSegue(withIdentifier: SegueIdentifier.addSegue, sender: nil)
    }
    
    private func setupNavigation(){
        
        navigationController?.navigationBar.isTranslucent = false
        
        navigationController!.navigationBar.setBackgroundImage(UIImage(), for: .default)
        let image = UIImage.imageWithColor(color: UIColor.white)
        navigationController!.navigationBar.shadowImage = image
        
        //Localize
        navigationItem.title = "Car List"
        
        let button = UIButton.init(type: .custom)
        button.setImage(#imageLiteral(resourceName: "plus"), for: .normal)
        button.addTarget(self, action:#selector(addCarAction), for: .touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        let barButton = UIBarButtonItem.init(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    
    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueIdentifier.addSegue{
            let backItem = UIBarButtonItem()
            //Localize
            backItem.title = NSLocalizedString("back", comment: "")
            navigationItem.backBarButtonItem = backItem
        }
        if segue.identifier == SegueIdentifier.showSegue {
            if let index = sender as? IndexPath{
                let object = fetchedResultsController.object(at: index)
                
                if let vc = segue.destination as? ShowCarViewController{
                    vc.selectedCar = object
                }
                
                
            }
        }
    }
}

extension CarListViewController: UITableViewDelegate, UITableViewDataSource{
    // MARK: tableView
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController.sections {
            return sections[section].numberOfObjects
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.tableCellCarId) as! CarTableViewCell
        
        let auto = fetchedResultsController.object(at: indexPath)
        
        
        cell.updateCell(withCar: auto)
        
        return cell
    }
    
    func configureCell(_ cell: CarTableViewCell, withCar auto: Car) {
        cell.updateCell(withCar: auto)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.headerIdentifier) as! CarListHeaderView
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.headerMainHeight
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.cellMainHeigt
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            
            let object = fetchedResultsController.object(at: indexPath)
            CoreDataManager.sharedInstance.managedObjectContext.delete(object)
            CoreDataManager.sharedInstance.saveContext()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: SegueIdentifier.showSegue, sender: indexPath)
    }
    
}


extension CarListViewController: NSFetchedResultsControllerDelegate{
    
    // MARK:  Fetched Results Controller Delegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            self.configureCell(tableView.cellForRow(at: indexPath!)! as! CarTableViewCell, withCar: anObject as! Car)
        case .move:
            tableView.moveRow(at: indexPath!, to: newIndexPath!)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
}

extension CarListViewController: WeatherDataModelDelegate{
    
    func update(withWeather weather: Weather, andDataModel: WeatherDataModel) {
        print(weather)
        
        if let header = tableView.headerView(forSection: 0) as? CarListHeaderView {
            header.update(forWeather: weather)
        }
    }
    
    func update(withCity city:String){
        if let header = tableView.headerView(forSection: 0) as? CarListHeaderView {
            header.cityLabel.text = city
        }
    }
    
    func showAlertForDeniedStatus() {
        print("showAlertForDeniedStatus")
        
        let alertController = UIAlertController(
            title: NSLocalizedString("Disabled", comment: ""),
            message: NSLocalizedString("Settings", comment: ""),
            preferredStyle: .alert)
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
            if let url = NSURL(string:UIApplicationOpenSettingsURLString) {
                UIApplication.shared.openURL(url as URL)
            }
        }
        alertController.addAction(openAction)
        
        if let header = tableView.headerView(forSection: 0) as? CarListHeaderView {
            if header.activityIndicator.isAnimating{
                header.activityIndicator.stopAnimating()
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showAlert(forNetworkError message: String) {
        
        
        let bannerView = UIView(frame: CGRect.init(x: 0, y: 0, width: view.frame.size.width, height: 44))
        bannerView.backgroundColor = UIColor.purple
        
        
        let label = UILabel(frame: bannerView.frame)
        label.text = message
        label.textColor = UIColor.white
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        
        bannerView.alpha = 0
        view.addSubview(bannerView)
        
        bannerView.addSubview(label)
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            bannerView.alpha = 1.0
            
        }) { (completion) in
            
            UIView.animate(withDuration: 0.5, delay: 2.0, usingSpringWithDamping: 1.0, initialSpringVelocity: 1.0, options: .curveEaseIn, animations: {
                bannerView.alpha = 0
                }, completion: { (completion) in
            })
        }
    }
}






