//
//  CarModel.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 16.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit
import CoreData

class CarModel: NSObject {
 
     var model: String = ""
     var price: String?
     var engine: String?
     var transmission: String?
     var condition: String?
     var descript: String?
     var date: NSDate = NSDate()
     var images: NSData?
    
    static func emptyCar() -> CarModel{
        return CarModel(model: "", price: "", engine: "", transmission: "", condition: "", descript: "", date: NSDate(), images: nil)
    }
    
    init(model: String,price: String?, engine: String?, transmission: String?, condition: String?, descript: String?, date: NSDate, images: NSData?){
        self.model = model
        self.price = price ?? ""
        self.engine = engine ?? ""
        self.transmission = transmission ?? ""
        self.condition = condition ?? ""
        self.descript = descript ?? ""
        self.date = date
        self.images = images
    }
    
}
