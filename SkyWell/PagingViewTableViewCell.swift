//
//  PagingViewTableViewCell.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 16.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit
protocol PageProtocol:class {
    func pageControlAction(sender:UIPageControl!)
}

class PagingViewTableViewCell: BaseCell {

    weak var delegate:PageProtocol?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func PageAction(_ sender: UIPageControl) {
        if self.delegate != nil{
            self.delegate?.pageControlAction(sender: sender)
        }
    }
    @IBOutlet var pageControl: UIPageControl!
 
    override func updateCell(withCar model:Car){
        self.backgroundColor = UIColor.greenMainColor()
        
        
        if let car = model.arrayOfImagesFor(){
            self.pageControl.numberOfPages = 0
            
            if  car.count > 0{
                self.pageControl.numberOfPages = car.count
            }
        }
    }
}
