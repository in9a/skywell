//
//  SubtitleTableViewCell.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 15.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

class SubtitleTableViewCell: BaseCell {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    @IBOutlet var textField: UITextField?
 
    
    func setUpCell(_ index:Int) {
        backgroundColor = UIColor.greenMainColor()
        
        switch index {
        case CellTypes.carModel.hashValue:
            titleLabel.text = NSLocalizedString("Model", comment: "")
            textField!.tag = index
            textField!.keyboardType = .default
        case CellTypes.carPrice.hashValue:
            titleLabel.text = NSLocalizedString("Price", comment: "")
            textField!.tag = index
            textField!.keyboardType = .decimalPad
        case CellTypes.engineCell.hashValue:
            titleLabel.text = NSLocalizedString("Engine", comment: "")
        case CellTypes.transmissionCell.hashValue:
            titleLabel.text = NSLocalizedString("Transmission", comment: "")
        case CellTypes.conditionCell.hashValue:
            titleLabel.text = NSLocalizedString("Condition", comment: "")
        default:break
        }
    }
}

 
