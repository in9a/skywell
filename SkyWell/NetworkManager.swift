//
//  NetworkManager.swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 14.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire


protocol NetworkManager {
    func getWeatherForCoordinate(_ coordinate :CLLocationCoordinate2D, success: @escaping WeatherCompletionBlock, failure:@escaping ErrorCompletionBlock)
}
typealias WeatherCompletionBlock = (Weather) -> ()
typealias ErrorCompletionBlock = (NSError) -> ()

class NetworkManagerImpl: NetworkManager {
    
    let apiKey = "718c6e6f6d51fd7dc635387dcb69256e"
    let weatherUrl = "http://api.openweathermap.org/data/2.5/weather/"
    
    let userLanguage: String = {
        if let language = NSLocale.preferredLanguages.first {
            
            switch language{
            case "ru": return "ru"
            case "ua": return "ua"
            default: return "en"
            }
        } else {
            return "en"
        }
    } ()
    
    let mesureSystem:MesureSystem = {
        if NSLocale.current.usesMetricSystem {
            return  .metric
        } else {
            return .imperial
        }
    }()
    
    internal func getWeatherForCoordinate(_ coordinate: CLLocationCoordinate2D, success: @escaping WeatherCompletionBlock, failure: @escaping ErrorCompletionBlock) {
        
        let parameters = ["lat" : String(coordinate.latitude),
                          "lon" : String(coordinate.longitude),
                          "APPID" : apiKey,
                          "units" : mesureSystem.stringValue(),
                          "lang" : userLanguage]
        
        Alamofire.request(weatherUrl, parameters: parameters).validate().responseJSON { (response) in
            
            switch response.result {
            case .success:
                if let json = response.result.value, let weatherJson = json as? [String: AnyObject] {
                    
                    let weatherData = Weather.decode(withJSON: weatherJson, forMesureSystem: self.mesureSystem)
                    
                    
                    success(weatherData)
                    
                }
                else {
                    let error = NSError(domain: "", code: 0, userInfo: nil)
                    
                    failure(error)
                    
                }
            case .failure(let error):
                
                failure(error as NSError)
                
            }
            }.resume()
    }
    
}






