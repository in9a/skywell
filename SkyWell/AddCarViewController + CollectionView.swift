//
//  AddCarViewController + .swift
//  SkyWell
//
//  Created by Anton Kharchevskyi on 15.09.16.
//  Copyright © 2016 Anton Kharchevskyi. All rights reserved.
//

import UIKit

extension AddCarViewController:UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedImageArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.collectionImageIdentifier, for: indexPath) as! ImageCollectionViewCell
        
        if indexPath.row == selectedImageArray.count {
            cell.isLast = true
        } else {
            cell.isLast = false
            cell.imageView.image = self.selectedImageArray[indexPath.row]
        }
        cell.layoutIfNeeded()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == selectedImageArray.count   {
            showPicker()
        }
    }
    
    func showPicker(){
        
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
        
        let picker = UIImagePickerController()
        picker.delegate = self
        
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            

            let makePhotoAction = UIAlertAction(title: NSLocalizedString("MakePhoto", comment: ""), style: .default) {[weak self] (action) in
                
                guard let strongSelf = self else {return}
                
                picker.allowsEditing = false
                picker.sourceType = .camera
                
                strongSelf.present(picker, animated: true, completion: nil)
                
            }
            alertController.addAction(makePhotoAction)
        }
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let chooseLibrary = UIAlertAction(title: NSLocalizedString("ChooseLibrary", comment: ""), style: .default) {[weak self] (action) in
                
                guard let strongSelf = self else {return}
                
                picker.allowsEditing = true
                picker.sourceType = .photoLibrary
                
                
                
                strongSelf.present(picker, animated: true, completion: nil)
                
            }
            alertController.addAction(chooseLibrary)
        }
        
        
        let cancel = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (alert) in  }
        
        alertController.addAction(cancel)
        
        
        self.present(alertController, animated: true) {  }
    }
}
extension AddCarViewController:UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let size: CGFloat = ceil(collecionHeight  * 0.8)
        
        return CGSize.init(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 5, 0, 0)
    }
}

extension AddCarViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var selectedImageFromPicker: UIImage?
        
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage {
            selectedImageFromPicker = editedImage
        }
        else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImageFromPicker = originalImage
        }
        if let selectedImage = selectedImageFromPicker {
            selectedImageArray.insert(selectedImage, at: 0)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
}

